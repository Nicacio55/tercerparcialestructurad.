/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenlazadastercerparcialdos;

/**
 *
 * @author Evert Luis Niacacio Pacheco   CI: 7450101
 */
public class listaDinamica<E> {
    
    private nodo<E> first;
    private nodo<E> last;
    private int size;

    public listaDinamica() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int sizeList() {
        return size;
    }

    private nodo<E> getNode(int index) {
        if (isEmpty() || (index < 0 || index >= sizeList())) {
            return null;
        } else if (index == 0) {
            return first;
        } else if (index == sizeList() - 1) {
            return last;
        } else {
            nodo<E> search = first;
            int count = 0;
            while (count < index) {
                count++;
                search = search.getNext();
            }
            return search;
        }
    }

    public E get(int index) {
        if (isEmpty() || (index < 0 || index >= sizeList())) {
            return null;
        } else if (index == 0) {
            return first.getElement();
        } else if (index == sizeList() - 1) {
            return last.getElement();
        } else {
            nodo<E> search = getNode(index);
            return search.getElement();
        }
    }

    public E getFirst() {
        if (isEmpty()) {
            return null;
        } else {
            return first.getElement();
        }
    }

    public E getLast() {
        if (isEmpty()) {
            return null;
        } else {
            return last.getElement();
        }
    }

    public E addFirst(E element) {
        nodo<E> newelement;
        if (isEmpty()) {
            newelement = new nodo<>(element, null);
            first = newelement;
            last = newelement;
        } else {
            newelement = new nodo<E>(element,first);
            first = newelement;
        }
        size++;
        return first.getElement();
    }

    public E addLast(E element) {
        nodo<E> newelement;
        if (isEmpty()) {
            return addFirst(element);
        } else {
            newelement = new nodo<E>(element,null);
            last.setNext(newelement);
            last = newelement;
        }
        size++;
        return last.getElement();
    }

    public E add(E element, int index) {
        if (index == 0) {
            return addFirst(element);
        } else if(index == sizeList()){
            return addLast(element);
        }else if((index<0 || index >= sizeList())){
            return null;
        }else{
            nodo<E> nodo_prev = getNode(index - 1);
            nodo<E> nodo_current = getNode(index);
            nodo<E> newelement = new nodo<>(element,nodo_current);
            nodo_prev.setNext(newelement);
            size++;
            return getNode(index).getElement();
        }
    }
    
    public String listContent(){
        String str = "";
        if(isEmpty()){
            str = "LISTA VACIA";
        }else{
           nodo<E> out = first;
           while(out != null){
               str += out.getElement()+" - ";
               out = out.getNext();
           }
        }
        return str;
    }
    
    public E removeFirst(){
        if(isEmpty()){
            return null;
        }else{
            E element = first.getElement();
            nodo<E> aux = first.getNext();
            first = aux;
            if(sizeList() == 1){
                last = null;
            }
            size--;
            return element;
        }
    }
    
    public E removeLast(){
        if(isEmpty()){
            return null;
        }else{
            E element = last.getElement();
            nodo<E> newLast = getNode(sizeList()-2);
            if(newLast == null){
                last = null;
                if(sizeList() == 2){
                    last = first;
                }else{
                    first = null;
                }
            }else{
                last = newLast;
                last.setNext(null);
            }
            size--;
            return element;
        }
    }
    
    public E remove(int index){
        if (index == 0) {
            return removeFirst();
        } else if(index == sizeList()){
            return removeLast();
        }else if(isEmpty() || (index<0 || index >= sizeList())){
            return null;
        }else{
            nodo<E> nodo_prev = getNode(index-1);
            nodo<E> nodo_current = getNode(index);
            nodo<E> nodo_current_next = nodo_current.getNext();
            E element = nodo_current.getElement();
            nodo_current = null;
            nodo_prev.setNext(nodo_current_next);
            size--;
            return element;
        }
    }
    public int indexOf(E element){
        if(isEmpty()){ 
            return -1;
        }else{
            nodo<E> aux = first; 
            int index = 0;
            while(aux != null){
                if(element == aux.getElement()){
                    return index;
                }
                index++;
                aux = aux.getNext();
            }
        }
        return -1;
    }
    public E modify(E newValue, int index){
        if(isEmpty() || (index<0 || index >= sizeList())){
            return null;
        }else{
            nodo<E> aux = getNode(index);
            aux.setElement(newValue);
            return aux.getElement();
        }
    } 

}
