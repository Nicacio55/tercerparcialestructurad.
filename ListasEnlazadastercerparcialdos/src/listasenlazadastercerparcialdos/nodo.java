/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenlazadastercerparcialdos;

/**
 *
 * @author Evert Luis Nicacio Pacheco   Ci: 7450101
 */
public class nodo<E> {
    private E element;
    private nodo<E> next;
    
    public nodo(E element, nodo<E> next){
        this.element = element;
        this.next = next;
    }

    public E getElement() {
        return element;
    }

    public void setElement(E element) {
        this.element = element;
    }

    public nodo<E> getNext() {
        return next;
    }

    public void setNext(nodo<E> next) {
        this.next = next;
    }

}
