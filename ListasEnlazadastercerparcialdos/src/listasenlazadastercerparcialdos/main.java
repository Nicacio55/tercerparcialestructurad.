/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenlazadastercerparcialdos;

/**
 *
 * @author Evert Luis Nicacio Pacheco    CI: 7450101
 */
public class main {
     public static void main(String[] args){
            listaDinamica<Integer> list = new listaDinamica<>();
            System.out.println(list.isEmpty());
            System.out.println("add elemento al ultimo "+list.addLast(2));
            System.out.println("add elemento al ultimo "+list.addLast(1));
            System.out.println("add elemento al ultimo "+list.addLast(3));
            System.out.println("add element  al principio"+list.addFirst(5));
            System.out.println("add elemento al ultimo "+list.addLast(4));
            System.out.println("add elemento al ultimo "+list.addLast(6));
            System.out.println("add element  al a la posicion 1"+list.add(7,2));
            System.out.println("add element  al a la posicion 1"+list.add(8,4));
            System.out.println("add element  al principio"+list.addFirst(9));
            System.out.println(list.listContent());
            System.out.println("eliminado el primero "+list.removeFirst());
            System.out.println("eliminado el ultimo "+list.removeLast());
            System.out.println(list.listContent());
            System.out.println("eliminado del index 2 "+list.remove(3));
            System.out.println(list.listContent());
            System.out.println("el index del numero 8 es :"+ list.indexOf(8));      
            System.out.println("el index del numero 10 es :"+ list.indexOf(10));  
            System.out.println("modificando el elemento del index  2"+ list.modify(9,2));
            System.out.println(list.listContent());
    }
}
