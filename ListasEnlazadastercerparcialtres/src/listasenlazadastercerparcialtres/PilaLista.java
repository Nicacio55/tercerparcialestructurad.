/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenlazadastercerparcialtres;

/**
 *
 * @author Evert Luis Nicacio Pacheco    CI : 7450101
 */
public class PilaLista {
    private Nodo cima;
public PilaLista(){
this.cima=null;
}
public Nodo getCima() {
return cima;
}
public void setCima(Nodo cima) {
this.cima = cima;
}
public void insertar(int dato){
Nodo i=new Nodo(dato);
i.setNext(this.cima);
this.cima=i;
}
public int extraer(){
int dato=cima.getDato();
this.cima=cima.getNext();
return dato;
}
public boolean estaVacia() {
if (cima==null) {
System.out.println("La pila esta vacia");
return true;
} else {
System.out.println("La pila no esta vacia");
return false;
}
}
public int contarDatos() {
int contador=0;
Nodo h=cima;
while (h!=null) {
contador++;
h=h.getNext();
}
System.out.println("Numero de datos en la pila son: "+contador);
return contador;
}
public String toString(){
String s="";
Nodo i=this.cima;
while(i!=null){
s=s+i.toString();
i=i.getNext();
}
return s;
}
}
