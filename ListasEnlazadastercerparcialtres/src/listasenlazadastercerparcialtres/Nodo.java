/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenlazadastercerparcialtres;

/**
 *
 * @author Evert Luis Nicacio Pacheco    CI : 7450101
 */
public class Nodo {
    private int dato;
private Nodo next;
public Nodo(int dato){
this.dato=dato;
}
public int getDato() {
return dato;
}
public void setDato(int dato) {
this.dato = dato;
}
public Nodo getNext() {
return next;
}
public void setNext(Nodo next) {
this.next = next;
}
public String toString(){
String s=" "+dato+" ";
return s;
}
}
